import Vuex from "vuex";
import cotizarMoto from "~/plugins/cotizacionMoto";
import getTokenService from "~/plugins/getToken";
import validacionesService from "../plugins/validaciones";
import dbService from "~/plugins/configBase";
import databaseMoto from "~/plugins/saveData";
import telApi from "~/plugins/telefonoService.js";
import cotizacionPromo from "~/plugins/descuentoService.js";

const createStore = () => {
  const validacion = new validacionesService();
  return new Vuex.Store({
    state: {
      cargandocotizacion: false,
      msj: false,
      msjEje: false,
      sin_token: false,
      tiempo_minimo: 1.3,
      config: {
        time:'',
        textoSMS:'',
        statusCot:false,
        imgen:'',
        habilitarBtnEmision: false,
        btnEmisionDisabled: false,
        interior: "",
        aseguradora: "",
        cotizacion: false,
        emision: false,
        descuento: 30,
        telefonoAS: "88902367",
        grupoCallback: "VN Agregadores",
        from: "DOMINIO-MIGO",
        idPagina: 0,
        asgNombre: "Migo Seguros",
        dominio: "migoseguros.com",
        k1: "",
        k2: "",
        k3: "",
        k4: "",
        k5: "",

        desc: "",
        msi: "",
        promoImg: "",
        promoLabel: "",
        promoSpecial: "false",
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0,
      },
      formData: {
        gclid_field: "",
        idLogData: "",
        idHubspot: '',
        urlOrigen: "",
        aseguradora: "MIGO",
        nombre: "",
        cp: "",
        telefono: "",
        correo: "",
        edad: "",
        genero: "",
        modelo: "",
        marca: "",
        submarca: "",
        detalle: "",
        descripcion: "",
        precio: "",
        tipo: "",
        clave: "",
        fechaNacimiento: "",
        emailValid: 1,
        telefonoValid: 1,
        codigoPostalValid: 1,
      },
      solicitud: {},
      cotizacion: {},
      servicios: {
        servicioDB: "http://138.197.128.236:8081/ws-autos/servicios",
      },
    },
    actions: {
      getToken(state) {
        return new Promise((resolve, reject) => {
          getTokenService.search(dbService.tokenData).then(
            (resp) => {
              if (typeof resp.data != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              } else {
                this.state.config.accessToken = resp.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            (error) => {
              reject(error);
            }
          );
        });
      },
    },
    mutations: {
      cotizacionPromo: function (state) {
        cotizacionPromo
          .search(state.config.aseguradora)
          .then((resp) => {
            resp = resp.data;
            if (resp.discount != "Sin Registro") {
              state.config.descuento = resp.discount;
            } else {
              state.config.descuento = 0;
            }
            if (parseInt(resp.data.delay)) {
              state.config.time = resp.data.delay
            } else {
              $store.state.config.time = 5000
             
            }
          })
          .catch((error) => {
            state.config.descuento = 0;
            state.config.time = 5000
          });
      },

      telApi: function (state) {
        try {
          telApi
            .search(state.config.idMedioDifusion)
            .then((resp) => {
              resp = resp.data;
              let telefono = resp.telefono;
              if (parseInt(telefono)) {
                var tel = telefono + "";
                var tel2 = tel.substring(2, tel.length);
                state.config.telefonoAS = tel2;
              }
            })
            .catch((error) => {
              console.log(error);
            });
        } catch (error) {
          console.log(error);
        }
      },
      validarTokenCore: function (state) {
        try {
          if (process.browser) {
            if (
              localStorage.getItem("authToken") === null ||
              localStorage.getItem("authToken") === "undefined"
            ) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              console.log("VALIDANDO TOKEN...");
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
    },
  });
};

export default createStore;
