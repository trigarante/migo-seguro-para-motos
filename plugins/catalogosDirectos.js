import axios from 'axios'

// let catalogo = process.env.catalogo + "/catalogos";
let urlConsumo = process.env.catalogo + '/v2/migo-motos'
// let urlConsumo = process.env.catalogo + '/v3/gnp-motos'


class Catalogos {
  marcas(accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + '/brands',
      headers: { Authorization: `Bearer ${accessToken}` },
      // params:{brand}
    })
  }
  modelos(brand, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + "/years",
      headers: { Authorization: `Bearer ${accessToken}` },
      params:{brand},
    });
  }
  submarcas(brand, year, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + '/models',
      headers: { Authorization: `Bearer ${accessToken}` },
      params:{brand,year}
    })
  }
  descripciones(brand, year, model, accessToken) {
    return axios({
      method: "get",
      url: urlConsumo + '/variants',
      headers: { Authorization: `Bearer ${accessToken}` },
      params:{brand,year,model}
    })
  }
}

export default Catalogos;
