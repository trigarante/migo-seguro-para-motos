import axios from 'axios'
import configDB from './configBase'

const cotizacionService = {}

cotizacionService.search = function (aseguradora,peticion,accessToken) {

  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    //COTIZACION SALESFORCE MOTOS
    // url: `https://core-persistance-service.com/v2/migos/motorcycle/quotation`,
    url:
      process.env.promoCore +
      `/v2/${aseguradora.toLowerCase()}/motorcycle/quotation`,
    data: JSON.parse(peticion),
  });
}
export default cotizacionService
