import axios from 'axios'

const url = process.env.coreBranding
const urlHubspot = process.env.hubspot
class SaveService {
    saveHubspot(peticion) {
        return axios({
            method: "post",
            headers: { 'Content-Type': 'application/json' },
            url: urlHubspot ,
            data: JSON.parse(peticion)
        })
    }
   
    saveProspecto(peticion, accessToken) {
        return axios({
            method: "post",
            headers: { 'Authorization': 'Bearer ' + accessToken },
            //branding salesForce
            url: process.env.promoCore + "/v3/cotizaciones/branding",
            data: JSON.parse(peticion)
        })
    }
    saveCotizacion(peticion, accessToken, cotizacionAli) {
        return axios({
            method: "put",
            headers: {'Authorization': 'Bearer '+accessToken},
             //ALi salesForce
             url: process.env.promoCore + `/v1/cotizaciones-ali/${cotizacionAli}`,
            data: JSON.parse(peticion)
        })
    }

}

export default SaveService