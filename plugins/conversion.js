const conversion ={}

conversion.gtag_report_conversion=function(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
    'send_to': 'AW-980827318/_0CGCLq5noEBELb52NMD',
    'event_callback': callback
  });
  gtag('event', 'conversion', {
        'send_to': 'AW-770989305/ytxoCPfny5IBEPm50e8C',
        'event_callback': callback
    });
  return false;
}
export default conversion
