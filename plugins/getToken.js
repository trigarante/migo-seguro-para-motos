import axios from 'axios'
import configDB from './configBase'

const getTokenService = {}

getTokenService.search = function () {
    return axios({
        method: "post",
        //url: configDB.baseUrl+  '/v1/authenticate',
        url: process.env.promoCore + '/v1/authenticate',
        data: {"tokenData": configDB.tokenData}
    })
        .then(res => res.data)
        .catch(err => err);
}
export default getTokenService